# Template Viber REST API Jersey 
 https://gitlab.com/Alex.UA/template-viber-rest-api-jersey/tree/dev

Template implemented interaction with Viber using [Viber REST API](https://developers.viber.com/docs/api/rest-bot-api/)

- Receiving, processing messages from Viber (subscribers)
- Send messages to Viber (subscribers)
- List of subscribers in the database
- [Get User Details](https://developers.viber.com/docs/api/rest-bot-api/#get-user-details)
- Save correspondence with the subscriber in a separate file
- Logging

Description sources :

 - entity - contains DTO
    - event - Callbacks (events), main classes start with E
    - message -  match message types, main classes start with M
    - keyboard - a custom [keyboard](https://viber.github.io/docs/tools/keyboards/) [(examples)](https://viber.github.io/docs/tools/keyboards/)
    - userDetails - corresponding get_user_details request
    - webhook - setting a Webhook 
- repository - contains classes for working with the database (H2), etc.
    - SubscriberRepository - subscribers repository (H2)
    - SubscriberCachRepository - cashing with ConcurrentHashMap 
    - UserDetailsDBRepository - user details repository ([Get User Details](https://developers.viber.com/docs/api/rest-bot-api/#get-user-details)) (H2)
    - ConversationRepository - save all event by subscribers in file,get
- security - basic access authentication filter
- service - handles events (callbacks), messages, subscriptions, etc.
- VServer.java - accepts messages from viber